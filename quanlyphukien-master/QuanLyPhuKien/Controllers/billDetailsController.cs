using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuanLyPhuKien.Models;

namespace QuanLyPhuKien.Controllers
{
    [AllowAnonymous]
    public class billDetailsController : Controller
    {
        private QuanLyPhuKienEntities5 db = new QuanLyPhuKienEntities5();

        // GET: billDetails
        public ActionResult Index()
        {
            var billDetails = db.billDetails.Include(b => b.bill).Include(b => b.product1);
            return View(billDetails.ToList());
        }

        // GET: billDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            billDetail billDetail = db.billDetails.Find(id);
            if (billDetail == null)
            {
                return HttpNotFound();
            }
            return View(billDetail);
        }

        // GET: billDetails/Create
        public ActionResult Create()
        {
            ViewBag.biilId = new SelectList(db.bills, "id", "id");
            ViewBag.product = new SelectList(db.products, "id", "name");
            return View();
        }

        // POST: billDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,product,quantity,biilId")] billDetail billDetail)
        {
            if (ModelState.IsValid)
            {
                db.billDetails.Add(billDetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.biilId = new SelectList(db.bills, "id", "id", billDetail.billId);
            ViewBag.product = new SelectList(db.products, "id", "name", billDetail.product);
            return View(billDetail);
        }

        // GET: billDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            billDetail billDetail = db.billDetails.Find(id);
            if (billDetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.biilId = new SelectList(db.bills, "id", "id", billDetail.billId);
            ViewBag.product = new SelectList(db.products, "id", "name", billDetail.product);
            return View(billDetail);
        }

        // POST: billDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,product,quantity,biilId")] billDetail billDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(billDetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.biilId = new SelectList(db.bills, "id", "id", billDetail.billId);
            ViewBag.product = new SelectList(db.products, "id", "name", billDetail.product);
            return View(billDetail);
        }

        // GET: billDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            billDetail billDetail = db.billDetails.Find(id);
            if (billDetail == null)
            {
                return HttpNotFound();
            }
            return View(billDetail);
        }

        // POST: billDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            billDetail billDetail = db.billDetails.Find(id);
            db.billDetails.Remove(billDetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
