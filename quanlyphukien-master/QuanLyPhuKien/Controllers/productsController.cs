using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuanLyPhuKien.Models;

namespace QuanLyPhuKien.Controllers
{
    [AllowAnonymous]
    public class productsController : Controller
    {
        private QuanLyPhuKienEntities5 db = new QuanLyPhuKienEntities5();

        // GET: products
        public ActionResult Index()
        {
            var products = db.products.Include(p => p.brand1).Include(p => p.category1);
            return View(products.ToList());
        }

        // GET: products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product product = db.products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: products/Create
        public ActionResult Create()
        {
            ViewBag.brand = new SelectList(db.brands, "id", "name");
            ViewBag.category = new SelectList(db.categories, "id", "name");
            return View();
        }

        // POST: products/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,img,price,category,description,monthOfGuaranty,brand")] product product)
        {
            if (ModelState.IsValid)
            {
                int fileCount = Request.Files.Count;
                if (fileCount > 0)
                {

                    for (int i = 0; i < fileCount; i++)
                    {
                        HttpPostedFileBase file = Request.Files[i]; //Uploaded file

                        if (file.ContentLength <= 0)
                        {
                            continue;
                        }

                        //save file
                        //E:\Dat\IT\C#\Asp.net\QuanLyPhuKien\QuanLyPhuKien\Content\img

                        var filePath = Server.MapPath("~/") + "Content/img/product/" + file.FileName;
                        filePath.Replace("#", "%23");
                        file.SaveAs(filePath);
                        product.img = file.FileName;
                    }
                    db.SaveChanges();
                }

                db.products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.brand = new SelectList(db.brands, "id", "name", product.brand);
            ViewBag.category = new SelectList(db.categories, "id", "name", product.category);
            return View(product);
        }

        // GET: products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product product = db.products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.brand = new SelectList(db.brands, "id", "name", product.brand);
            ViewBag.category = new SelectList(db.categories, "id", "name", product.category);
            return View(product);
        }

        // POST: products/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,img,price,category,description,monthOfGuaranty,brand")] product product)
        {
            if (ModelState.IsValid)
            {
                int fileCount = Request.Files.Count;
                if (fileCount > 0)
                {

                    for (int i = 0; i < fileCount; i++)
                    {
                        HttpPostedFileBase file = Request.Files[i]; //Uploaded file

                        if (file.ContentLength <= 0)
                        {
                            continue;
                        }

                        //save file
                        //E:\Dat\IT\C#\Asp.net\QuanLyPhuKien\QuanLyPhuKien\Content\img

                        var filePath = Server.MapPath("~/") + "Content/img/product/" + file.FileName;
                        filePath.Replace("#", "%23");
                        file.SaveAs(filePath);
                        product.img = file.FileName;
                    }
                    db.SaveChanges();
                }

                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.brand = new SelectList(db.brands, "id", "name", product.brand);
            ViewBag.category = new SelectList(db.categories, "id", "name", product.category);
            return View(product);
        }

        // GET: products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product product = db.products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            product product = db.products.Find(id);
            db.products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
