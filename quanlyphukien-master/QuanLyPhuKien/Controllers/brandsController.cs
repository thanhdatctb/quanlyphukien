﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using QuanLyPhuKien.Models;

namespace QuanLyPhuKien.Controllers
{
    [Authorize]
    public class brandsController : Controller
    {
        private QuanLyPhuKienEntities5 db = new QuanLyPhuKienEntities5();

        // GET: brands
        public ActionResult Index()
        {
            return View(db.brands.ToList());
        }

        // GET: brands/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            brand brand = db.brands.Find(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(brand);
        }

        // GET: brands/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: brands/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,ImageFile,description")] brand brand)
        {

            if (ModelState.IsValid)
            {
                int fileCount = Request.Files.Count;
                if (fileCount > 0)
                {

                    for (int i = 0; i < fileCount; i++)
                    {
                        HttpPostedFileBase file = Request.Files[i]; //Uploaded file

                        if (file.ContentLength <= 0)
                        {
                            continue;
                        }
                        
                        //save file
                        //E:\Dat\IT\C#\Asp.net\QuanLyPhuKien\QuanLyPhuKien\Content\img

                        var filePath = Server.MapPath("~/") + "Content/img/brand/" + file.FileName;
                        filePath.Replace("#", "%23");
                        file.SaveAs(filePath);
                        brand.img = file.FileName;
                    }
                    db.SaveChanges();
                }

                db.brands.Add(brand);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(brand);
        }

        // GET: brands/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            brand brand = db.brands.Find(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(brand);
        }

        // POST: brands/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,img,description")] brand brand)
        {
            if (ModelState.IsValid)
            {
                int fileCount = Request.Files.Count;
                for (int i = 0; i < fileCount; i++)
                {
                    HttpPostedFileBase file = Request.Files[i]; //Uploaded file

                    if (file.ContentLength <= 0)
                    {
                        continue;
                    }

                    //save file
                    //E:\Dat\IT\C#\Asp.net\QuanLyPhuKien\QuanLyPhuKien\Content\img

                    var filePath = Server.MapPath("~/") + "Content/img/brand/" + file.FileName;
                    filePath.Replace("#", "%23");
                    file.SaveAs(filePath);
                    brand.img = file.FileName;
                }
                db.Entry(brand).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(brand);
        }

        // GET: brands/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            brand brand = db.brands.Find(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(brand);
        }

        // POST: brands/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            brand brand = db.brands.Find(id);
            db.brands.Remove(brand);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
