﻿using QuanLyPhuKien.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyPhuKien.Controllers
{
    public class HomeController : Controller
    {
        private QuanLyPhuKienEntities5 db = new QuanLyPhuKienEntities5();
        public ActionResult Index()
        {
            dynamic mymodel = new ExpandoObject();
            mymodel.Catogories = db.categories.ToList();
            mymodel.Brands = db.brands.ToList();
            mymodel.Products = db.products.ToList();
            mymodel.Customer = (customer) HttpContext.Session["customer"];
            return View(mymodel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        
    }
}