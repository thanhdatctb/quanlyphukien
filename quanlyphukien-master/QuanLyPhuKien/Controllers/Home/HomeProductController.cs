﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace QuanLyPhuKien.Controllers.Home
{
    public class HomeProductController : Controller
    {
        private QuanLyPhuKienEntities5 db = new QuanLyPhuKienEntities5();
        // GET: Product
        public ActionResult Index()
        {
            List<product> products = new List<product>();
            products = db.products.ToList();
            var categories = db.categories.ToList();
            dynamic mymodel = new ExpandoObject();
            mymodel.Products = products;
            mymodel.Categories = categories;
            return View(mymodel);
            
        }

        // GET: Product/Details/5
        public ActionResult Detail(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product product = db.products.Find(id);
            List<product> relateProduct = new List<product>();
            List<product> brand = db.products.Where(e => e.brand == product.brand).ToList();
            List<product> cat = db.products.Where(e => e.category == product.category).ToList();
            List<comment> comments = db.comments.Where(e => e.product == product.id).ToList() ;
            int count = 0;
            foreach(var item in cat)
            {
                if(count < 3)
                {
                    relateProduct.Add(item);
                    count++;
                }    
            }
            count = 0;
            foreach (var item in brand)
            {
                if (count < 3)
                {
                    relateProduct.Add(item);
                    count++;
                }
            }
            if (product == null)
            {
                return HttpNotFound();
            }
            dynamic mymodel = new ExpandoObject();

            mymodel.Product = product;
            mymodel.RelateProduct = relateProduct;
            mymodel.Comments = comments;
            return View(mymodel);
            
            
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Product/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Product/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
