﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyPhuKien.Controllers.Home
{
    public class HomeBrandController : Controller
    {
        QuanLyPhuKienEntities5 db = new QuanLyPhuKienEntities5();
        public ActionResult Product(int id)
        {
            List<product> products = new List<product>();
            products = db.products.Where(e => e.brand == id).ToList();
            var categories = db.categories.ToList();
            dynamic mymodel = new ExpandoObject();
            mymodel.Products = products;
            mymodel.Categories = categories;
            mymodel.Brand = db.brands.Find(id);
            return View(mymodel);
        }
        // GET: HomeBrand
        public ActionResult Index()
        {
            return View();
        }

        // GET: HomeBrand/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: HomeBrand/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HomeBrand/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: HomeBrand/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }
        
        // POST: HomeBrand/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: HomeBrand/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: HomeBrand/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
